from matplotlib import pyplot as plt
import numpy as np


def sigmoid(x):
    return 1 / (1 + np.exp(-x))


def sigmoid_p(x):
    return sigmoid(x) * (1 - sigmoid(x))


# T = np.linspace(-20, 20, 100)
#
# plt.plot(T, sigmoid(T), c='r')
# plt.plot(T, sigmoid_p(T), c='b')
# plt.show()
