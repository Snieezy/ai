from matplotlib import pyplot as plt
import numpy as np

from main import X_SIZE, Y_SIZE
from sigmoid import sigmoid_p, sigmoid

data = [[3, 1.5, 1],
        [2, 1, 0],
        [4, 1.5, 1],
        [3, 1, 0],
        [3.5, 0.5, 1],
        [2, 0.5, 0],
        [5.5, 1, 1],
        [1, 1, 0]]

mystery_flower = [4.5, 1]

w1 = np.random.randn()
w2 = np.random.randn()
b = np.random.randn()

print("Initial random values:")
costs = []
print("w1 = ", w1)
print("w2 = ", w2)
print("b = ", b)

learning_rate = 0.1
number_of_epochs = 1000000

for _ in range(number_of_epochs):
    ri = np.random.randint(len(data))
    point = data[ri]

    z = point[0] * w1 + point[1] * w2 + b
    pred = sigmoid(z)
    target = point[2]
    cost = np.square(pred - target)

    costs.append(cost)

    dcost_pred = 2 * (pred - target)
    dpred_dz = sigmoid_p(z)

    dz_dw1 = point[0] #length
    dz_dw2 = point[1] #width
    dz_db = 1

    dcost_dw1 = dcost_pred * dpred_dz * dz_dw1
    dcost_dw2 = dcost_pred * dpred_dz * dz_dw2
    dcost_db = dcost_pred * dpred_dz * dz_db

    w1 = w1 - learning_rate * dcost_dw1
    w2 = w2 - learning_rate * dcost_dw2
    b = b - learning_rate * dcost_db

plt.plot(costs)
plt.show()
plt.gcf().clear()

print()
print("After approximation:")

print("w1 = ", w1)
print("w2 = ", w2)
print("b = ", b)


def get_color(m1, m2):
    return sigmoid(w1 * m1 + w2 * m2 + b)


plt.axis([0, X_SIZE, 0, Y_SIZE])  # x_min, x_max, y_min, y_max

details_rate = .1


for x in np.arange(0, X_SIZE, details_rate):
    for y in np.arange(0, Y_SIZE, details_rate):
        col_number = get_color(x, y)
        color = '#ff9999'
        if col_number < 0.5:
            color = '#99ccff'
        plt.scatter(x, y, c=color)


def add_graph_solution():
    for i in range(len(data)):
        p = data[i]
        col = 'r'
        if p[2] == 0:
            col = 'b'
        plt.scatter(p[0], p[1], c=col)
    plt.scatter(mystery_flower[0], mystery_flower[1], c='g')


add_graph_solution()
plt.show()

# print()
# print("Final result:")
# print(sigmoid(get_color(mystery_flower[0], mystery_flower[1])))
