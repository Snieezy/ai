from keras.models import Sequential
from keras.layers import Dense
from keras.optimizers import Adam
from matplotlib import pyplot as plt
import numpy as np

import numpy

# fix random seed for reproducibility
from main import X_SIZE, Y_SIZE

numpy.random.seed(7)

data = [[3, 1.5, 1],
        [2, 1, 0],
        [4, 1.5, 1],
        [3, 1, 0],
        [3.5, 0.5, 1],
        [2, 0.5, 0],
        [5.5, 1, 1],
        [1, 1, 0]]

mystery_flower = [[4.5, 1]]
mystery_flower = numpy.array(mystery_flower)

X = [[point[0], point[1]] for point in data]
Y = [point[2] for point in data]

X = numpy.array(X)
Y = numpy.array(Y)

model = Sequential()

model.add(Dense(2, activation='relu'))  # number of neurons, activation function
model.add(Dense(1, activation='sigmoid'))

optimizer = Adam(lr=0.1)
model.compile(loss='logcosh', optimizer=optimizer, metrics=['accuracy'])

model.fit(X, Y, epochs=200, batch_size=1, verbose=0)

colors = model.evaluate(X, Y)

print("Prediction:", model.predict(mystery_flower))

plt.axis([0, X_SIZE, 0, Y_SIZE])  # x_min, x_max, y_min, y_max

details_rate = .1

x_values = [[x, y] for x in np.arange(0, X_SIZE, details_rate) for y in np.arange(0, Y_SIZE, details_rate)]

x_values = numpy.array(x_values)

y_values = model.predict(x_values)

for idx, pred in enumerate(y_values):
    col_number = pred[0]
    color = '#ff9999'
    if col_number < 0.5:
        color = '#99ccff'
    plt.scatter(x_values[idx][0], x_values[idx][1], c=color)


def add_graph_solution():
    for i in range(len(data)):
        p = data[i]
        col = 'r'
        if p[2] == 0:
            col = 'b'
        plt.scatter(p[0], p[1], c=col)
    plt.scatter(mystery_flower[0][0], mystery_flower[0][1], c='g')


add_graph_solution()
plt.show()