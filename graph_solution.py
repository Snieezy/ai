from matplotlib import pyplot as plt

from main import X_SIZE, Y_SIZE

data = [[3, 1.5, 1],
        [2, 1, 0],
        [4, 1.5, 1],
        [3, 1, 0],
        [3.5, 0.5, 1],
        [2, 0.5, 0],
        [5.5, 1, 1],
        [1, 1, 0]]

mystery_flower = [4.5, 1]

plt.axis([0, X_SIZE, 0, Y_SIZE])  # x_min, x_max, y_min, y_max

def add_graph_solution():
    for i in range(len(data)):
        point = data[i]
        color = 'r'
        if point[2] == 0:
            color = 'b'
        plt.scatter(point[0], point[1], c=color)
    plt.scatter(mystery_flower[0], mystery_flower[1], c='g')


add_graph_solution()
plt.show()
