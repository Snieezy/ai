import numpy as np

from sigmoid import sigmoid

X_SIZE = 6
Y_SIZE = 6

data = [[3, 1.5, 1],
        [2, 1, 0],
        [4, 1.5, 1],
        [3, 1, 0],
        [3.5, 0.5, 1],
        [2, 0.5, 0],
        [5.5, 1, 1],
        [1, 1, 0]]

mystery_flower = [4.5, 1]

w1 = .3063203662727242
w2 = .6640565802177396
b = .606818771647744

def get_color(m1, m2):
    return sigmoid(w1 * m1 + w2 * m2 + b)

# print(get_color(2, 1))
